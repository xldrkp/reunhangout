import React from "react";
import {connect} from "react-redux";
import * as style from "../../../scss/pages/plenary/_breakoutliststyle.scss";
import * as BS from "react-bootstrap";
import * as A from "../actions";
import Breakout from './Breakout';

class BreakoutList extends React.Component {
  constructor(props) {
    super(props);
    this.state= {
      "create-session-dialog": false,
      "message-breakouts-dialog": false,
      "breakout-mode-dialog": false,
      "breakout_mode": this.props.plenary.breakout_mode,
      "max_attendees": 10,
      "random_max_attendees": this.props.plenary.random_max_attendees || 10,
    }
  }
  componentWillReceiveNewProps(newProps) {
    this.setState({"breakout_mode": newProps.plenary.breakout_mode})
  }
  handleCreateBreakout(event, isProposal) {
    event.preventDefault();
    if (!this.state.title) {
      this.setState({"title-error": "Title is required"});
      return;
    } else if (this.state.title.length > 100) {
      this.setState({"title-error": "Maximum length 100 characters"});
      return;
    } else {
      this.setState({"title-error": ""});
    }
    let maxAttendees = parseInt(this.state.max_attendees);
    if (isNaN(maxAttendees) || maxAttendees < 2 || maxAttendees > 10) {
      this.setState({"max_attendees-error": "Must be a number between 2 and 10."});
      return;
    } else {
      this.setState({"max_attendees-error": ""});
    }
    this.props.onChangeBreakouts({
      action: "create",
      title: this.state.title,
      max_attendees: this.state.max_attendees,
      is_proposal: isProposal,
    });
    this.setState({
      title: "",
      'create-session-dialog': false,
    });
  }

  handleModeChange(event) {
    event.preventDefault();
    let update = {breakout_mode: this.state.breakout_mode};
    if (update.breakout_mode === "random") {
      let maxAttendees = parseInt(this.state.random_max_attendees);
      if (isNaN(maxAttendees) || maxAttendees < 2 || maxAttendees > 10) {
        this.setState({
          "random_max_attendees-error": "Must be a number between 2 and 10."
        });
        return;
      } else {
        this.setState({"random_max_attendees-error": ""});
      }
      update.random_max_attendees = this.state.random_max_attendees;
    }
    this.props.onChangeBreakoutMode({breakout_mode: this.state.breakout_mode})
    this.setState({"breakout-mode-dialog": false})
  }

  handleMessageBreakouts(event) {
    event.preventDefault();
    if (this.state.breakoutMessage) {
      this.props.onMessageBreakouts({
        message: `${this.props.auth.display_name}: ${this.state.breakoutMessage}`
      })
      this.setState({"message-breakouts-dialog": false});
    }
  }

  handleGroupMe(event) {
    event.preventDefault();
    this.props.onChangeBreakouts({action: 'group_me'});
  }

  sortBreakouts(breakouts) {
    let approved = breakouts.filter((b) => {
      return !b.is_proposal
    })
    let unapproved = breakouts.filter((b) => {
      return b.is_proposal
    })
    return [...approved, ...unapproved]
  }

  renderModalForm(name, title, body, actionName, handler) {
    return <div className={name}>
      <BS.Modal show={this.state[name]}
                onHide={() => this.setState({[name]: false})}>
        <BS.Modal.Header closeButton>
          <BS.Modal.Title>{title}</BS.Modal.Title>
        </BS.Modal.Header>
        <BS.Form onSubmit={handler}>
          <BS.Modal.Body className='form-horizontal'>
            {body}
          </BS.Modal.Body>
        </BS.Form>
        <BS.Modal.Footer>
          <BS.Button onClick={() => this.setState({[name]: false})}>
            Close
          </BS.Button>
          <BS.Button bsStyle='primary' onClick={handler}>{actionName}</BS.Button>
        </BS.Modal.Footer>
      </BS.Modal>
    </div>
  }

  render() {
    let breakout_mode = this.props.plenary.breakout_mode
    let breakoutFilter;
    switch (breakout_mode) {
      case "admin":
        breakoutFilter = (b) => !b.is_proposal && !b.is_random;
        break;
      case "user":
        breakoutFilter = (b) => !b.is_random;
        break;
      case "random":
        breakoutFilter = (b) => b.is_random && (
          this.props.auth.is_superuser ||
          !!_.find(b.members, (m) => m.username === this.props.auth.username)
        );
        break;
    }
    let breakouts = this.props.breakouts.filter(breakoutFilter);
    if (breakout_mode === "user") {
      breakouts = this.sortBreakouts(breakouts)
    }

    let showCreateSession = (
      breakout_mode === "user" ||
      (this.props.auth.is_admin && breakout_mode === "admin")
    );

    return <div className='breakout-list-component'>
      <div className="breakout-list-header">
        <span className='pull-right'>
          { showCreateSession ?
              <BS.OverlayTrigger placement='left' overlay={
                <BS.Tooltip id='add-breakout-tooltip'>
                  {breakout_mode === "user" ? "Propose breakout" : "Add breakout"}
                </BS.Tooltip>
              }>
                <BS.Button onClick={() => this.setState({"create-session-dialog": true})}
                    className="create-session-btn">
                  <BS.Glyphicon glyph="plus" />
                </BS.Button>
              </BS.OverlayTrigger>
            : "" }
          { breakout_mode === "random" ?
              <BS.Button onClick={(e) => this.handleGroupMe(e)}>
                { breakouts.length === 0 ? "Group me" : "Regroup me" }
              </BS.Button>
            : "" }
          { this.props.auth.is_admin ?
              <BS.OverlayTrigger placement='left' overlay={
                <BS.Tooltip id='configure-breakouts-tooltip'>
                  Configure Breakouts
                </BS.Tooltip>
              }>
                <BS.Dropdown
                  id="user-menu-button"
                  pullRight>
                  <BS.Dropdown.Toggle
                    noCaret
                    className="breakout-settings-btn">
                    <img src="../../../../media/assets/control-panel-icon" />
                  </BS.Dropdown.Toggle>
                  <BS.Dropdown.Menu>
                    <BS.MenuItem
                      onClick={() => this.setState({"message-breakouts-dialog": true})}>
                      Message All Breakouts
                    </BS.MenuItem>
                    <BS.MenuItem
                      onClick={() => this.setState({"breakout-mode-dialog": true})}>
                      Breakout Mode
                    </BS.MenuItem>
                  </BS.Dropdown.Menu>
                </BS.Dropdown>
              </BS.OverlayTrigger>
            : "" }
        </span>
        <h4>Breakout Rooms</h4>
        { this.props.breakoutCrud.error ?
            <div className="breakout-error">
              {this.props.breakoutCrud.error.message}
            </div>
          : "" }
      </div>

      <div className="breakout-list-container">
        { breakouts.map((breakout, i) => {
            return <Breakout
              plenary={this.props.plenary}
              breakout={breakout}
              presence={this.props.breakout_presence[breakout.id] || {}}
              auth={this.props.auth}
              key={`${i}`}
              onChangeBreakouts={this.props.onChangeBreakouts} />
          })
        }
      </div>

      { this.renderModalForm(
          "create-session-dialog",
          "Create Session",
          <div>
            <BS.FormGroup controlId="session-name" className='non-margined'
                validationState={this.state['title-error'] ? 'error' : undefined}>
              <BS.ControlLabel>Session Name</BS.ControlLabel>
              <BS.FormControl type="text"
                  placeholder="Session Name"
                  title={(this.state && this.state.title) || ""}
                  onChange={(e) => this.setState({title: e.target.value})} />
              { this.state['title-error'] ?
                  <BS.HelpBlock>{this.state['title-error']}</BS.HelpBlock>
                : "" }
            </BS.FormGroup>
            <BS.FormGroup
              className='non-margined'
              controlId="participant-limit"
              validationState={
                this.state['max_attendees-error'] ? 'error' : undefined
              }
            >
              <BS.ControlLabel>Participant Limit</BS.ControlLabel>
              <BS.FormControl type="text"
                placeholder="Max 10, Min 2"
                min={2}
                max={10}
                value={this.state.max_attendees || ""}
                onChange={(e) => this.setState({max_attendees: e.target.value})} />
              <BS.HelpBlock>
                { this.state['max_attendees-error'] ?
                    this.state['max_attendees-error']
                  : "Minimum 2, Maximum 10" }
              </BS.HelpBlock>
            </BS.FormGroup>
          </div>,
          breakout_mode === "user" ? "Propose Session" : "Create Session",
          (e) => this.handleCreateBreakout(e, breakout_mode === "user")
        )
      }

      { this.renderModalForm(
          "message-breakouts-dialog",
          "Message All Breakouts",
          <BS.FormGroup controlId='breakout-message' className='non-margined'>
            <BS.HelpBlock>
              Send a text message to all breakout groups.
            </BS.HelpBlock>
            <BS.ControlLabel>Message</BS.ControlLabel>
            <BS.FormControl
              componentClass='textarea'
              placeholder='Message'
              value={this.state.breakoutMessage}
              onChange={(e) => this.setState({breakoutMessage: e.target.value})} />
            <h4>Preview</h4>
            <div className='breakout-message-preview'>
              {this.props.auth.display_name}: {this.state.breakoutMessage}
            </div>
          </BS.FormGroup>,
          "Send Message",
          (e) => this.handleMessageBreakouts(e)
        ) }

        { this.renderModalForm(
            "breakout-mode-dialog",
            "Breakout Mode",
            <div>
              <BS.FormGroup controlId="admin-mode" className='non-margined'>
                <BS.Radio
                    checked={this.state.breakout_mode==="admin"}
                    onChange={() => this.setState({"breakout_mode": "admin"})}>
                  Admin Proposed Sessions
                </BS.Radio>
                <BS.HelpBlock>
                  Admins can create breakout sessions
                </BS.HelpBlock>
                <BS.Radio checked={this.state.breakout_mode==="user"}
                    onChange={() => this.setState({"breakout_mode": "user"})}>
                  Participant Proposed Sessions
                </BS.Radio>
                <BS.HelpBlock>
                  All users can propose breakout sessions which can be voted on
                  by all users.  Admins can approve these sessions after which
                  users can join the sessions.
                </BS.HelpBlock>
                <BS.Radio checked={this.state.breakout_mode==="random"}
                    onChange={() => this.setState({"breakout_mode": "random"})}>
                  Randomly Assigned Sessions
                </BS.Radio>
                <BS.HelpBlock>
                  Users are randomly assigned to breakout sessions and will
                  always be in the same breakout session.  Users are given the
                  option of leaving their group and joining a new one with the
                  "Regroup Me" button.
                </BS.HelpBlock>
              </BS.FormGroup>
              { this.state.breakout_mode === "random" ?
                <BS.FormGroup
                  className='non-margined'
                  controlId="participant-limit"
                  validationState={
                    this.state['random_max_attendees-error'] ? 'error' : undefined
                  }
                >
                  <BS.ControlLabel>
                    Maximum number of participants per breakout
                  </BS.ControlLabel>
                  <BS.FormControl
                    type="text"
                    placeholder="Max 10, Min 2"
                    value={this.state.random_max_attendees}
                    onChange={
                      (e) => this.setState({random_max_attendees: e.target.value})
                    } />
                  { this.state['random_max_attendees-error'] ?
                      <BS.HelpBlock>
                        {this.state['random_max_attendees-error']}
                      </BS.HelpBlock>
                    : "" }
                </BS.FormGroup>
                : ""
              }
            </div>,
            "Set",
            (e) => this.handleModeChange(e)
          ) }
    </div>
  }
}

export default connect(
  // map state to props
  (state) => ({
    breakouts: state.breakouts,
    breakoutCrud: state.breakoutCrud,
    plenary: state.plenary,
    auth: state.auth,
    breakout_presence: state.breakout_presence,
  }),
  (dispatch, ownProps) => ({
    onChangeBreakouts: (payload) => dispatch(A.changeBreakouts(payload)),
    onChangeBreakoutMode: (payload) => dispatch(A.changeBreakoutMode(payload)),
    onMessageBreakouts: (payload) => dispatch(A.messageBreakouts(payload)),
  })
)(BreakoutList);
